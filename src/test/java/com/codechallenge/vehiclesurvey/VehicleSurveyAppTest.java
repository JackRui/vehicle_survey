package com.codechallenge.vehiclesurvey;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class VehicleSurveyAppTest {
    @Test
    public void constructor_givenFile_createsApp(){
        VehicleSurveyApp app = new VehicleSurveyApp("sample.txt");
        assertEquals("sample.txt",app.getInputFile());
    }

    @Test
    public void constructor_givenNothing_createsApp(){
        VehicleSurveyApp app = new VehicleSurveyApp();
        assertTrue(app.getInputFile().isEmpty());
    }
}
