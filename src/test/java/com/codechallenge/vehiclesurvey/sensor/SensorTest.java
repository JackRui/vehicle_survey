package com.codechallenge.vehiclesurvey.sensor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class SensorTest {

    @Test
    public void valueOfSensor_givenExpectedValues_convertsToSensor() {
        assertNotNull(Sensor.valueOf("A"));
        assertNotNull(Sensor.valueOf("B"));
    }
}
