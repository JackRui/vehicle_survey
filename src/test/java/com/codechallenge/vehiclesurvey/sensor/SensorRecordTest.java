package com.codechallenge.vehiclesurvey.sensor;

import org.junit.jupiter.api.Test;

import java.time.DateTimeException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SensorRecordTest {

    @Test
    public void constructor_givenValidSensorNumberAndTime_createsObject() {

        final SensorRecord record1 = new SensorRecord("A", 12345);
        assertEquals(Sensor.A, record1.getSensor());
        assertEquals(12345, record1.getMillisecond());
        assertEquals("00:00:12.345", record1.getLocalTime().toString());

        //max milliseconds of a day
        final SensorRecord record2 = new SensorRecord("B", 86399999);
        assertEquals(Sensor.B, record2.getSensor());
        assertEquals(86399999, record2.getMillisecond());
        assertEquals("23:59:59.999", record2.getLocalTime().toString());
    }

    @Test
    public void constructor_givenInvalidSensorNumber_throwException() {
        assertThrows(IllegalArgumentException.class, () -> new SensorRecord("C", 100));
    }

    @Test
    public void constructor_givenInvalidMillisecondsOfDay_throwException() {
        assertThrows(DateTimeException.class, () -> new SensorRecord("A", 86400000));

        assertThrows(DateTimeException.class, () -> new SensorRecord("A", -1));
    }

    @Test
    public void parse_givenCorrectValue_returnTrue() throws InvalidSensorRecordFormatException {
        SensorRecord record1 = SensorRecord.parse("A100");
        assertEquals(Sensor.A, record1.getSensor());
        assertEquals(100, record1.getMillisecond());

        SensorRecord record2 = SensorRecord.parse("B132133");
        assertEquals(Sensor.B, record2.getSensor());
        assertEquals(132133, record2.getMillisecond());
    }

    @Test
    public void parse_givenInvalidValueFormat_throwsException() {
        assertThrows(InvalidSensorRecordFormatException.class, ()->SensorRecord.parse(""));
        assertThrows(InvalidSensorRecordFormatException.class, ()->SensorRecord.parse("Aqwert"));
        assertThrows(InvalidSensorRecordFormatException.class, ()->SensorRecord.parse("C12345"));
        assertThrows(InvalidSensorRecordFormatException.class, ()->SensorRecord.parse("A999999999999999999999999"));
    }


}
