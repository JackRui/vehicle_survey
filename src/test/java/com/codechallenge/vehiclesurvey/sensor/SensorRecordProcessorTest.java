package com.codechallenge.vehiclesurvey.sensor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SensorRecordProcessorTest {

    private ArrayList<SensorRecord> sequentialRecords;
    private ArrayList<SensorRecord> simpleConcurrentRecords;
    private SensorRecordProcessor processor;

    @BeforeEach
    public void setUp() {
        processor = new SensorRecordProcessor();
        sequentialRecords = new ArrayList<>();
        sequentialRecords.addAll(Arrays.asList(
                new SensorRecord("A", 86390000), // car 1 axle 1
                new SensorRecord("A", 86390500), // car 1 axle 2
                new SensorRecord("A", 86398000), // car 2
                new SensorRecord("B", 86398100),
                new SensorRecord("A", 2000),
                new SensorRecord("B", 2100)));

        simpleConcurrentRecords = new ArrayList<>();
        simpleConcurrentRecords.addAll(Arrays.asList(
                new SensorRecord("A", 100), // car 1 axle 1
                new SensorRecord("A", 295), // car 2 axle 1A
                new SensorRecord("A", 300), // car 1 axle 2
                new SensorRecord("B", 305), // car 2 axle 1B
                new SensorRecord("A", 495),
                new SensorRecord("B", 505)));
    }

    @Test
    public void identifyRedundantAFromB_givenSequentialRecords_findsMatchedA() throws InvalidSensorRecordNumberException {
        ArrayList<SensorRecord> matchedRecords =
                processor.identifyRedundantAFromB(sequentialRecords, 3, 5);
        assertEquals(2, matchedRecords.size());
        assertEquals(sequentialRecords.get(2), matchedRecords.get(0));
        assertEquals(sequentialRecords.get(4), matchedRecords.get(1));
    }

    @Test
    public void identifyRedundantAFromB_givenSimpleConcurrentRecords_findsMatchedA() throws InvalidSensorRecordNumberException {
        ArrayList<SensorRecord> matchedRecords =
                processor.identifyRedundantAFromB(simpleConcurrentRecords, 3, 5);
        assertEquals(2, matchedRecords.size());
        assertEquals(simpleConcurrentRecords.get(1), matchedRecords.get(0));
        assertEquals(simpleConcurrentRecords.get(4), matchedRecords.get(1));
    }

    @Test
    public void prepare_givenSequentialRecords_removesRedundantA() throws InvalidSensorRecordNumberException {
        ArrayList<SensorRecord> rawRecords = (ArrayList<SensorRecord>) sequentialRecords.clone();
        ArrayList<SensorRecord> preparedRecords = processor.prepare(sequentialRecords);
        assertEquals(4, preparedRecords.size());
        assertEquals(rawRecords.get(0), preparedRecords.get(0));
        assertEquals(rawRecords.get(1), preparedRecords.get(1));
        assertEquals(rawRecords.get(3), preparedRecords.get(2));
        assertEquals(rawRecords.get(5), preparedRecords.get(3));
    }

    @Test
    public void prepare_givenSimpleConcurrentRecords_removesRedundantA() throws InvalidSensorRecordNumberException {
        ArrayList<SensorRecord> rawRecords = (ArrayList<SensorRecord>) simpleConcurrentRecords.clone();
        ArrayList<SensorRecord> preparedRecords = processor.prepare(simpleConcurrentRecords);
        assertEquals(4, preparedRecords.size());
        assertEquals(rawRecords.get(0), preparedRecords.get(0));
        assertEquals(rawRecords.get(2), preparedRecords.get(1));
        assertEquals(rawRecords.get(3), preparedRecords.get(2));
        assertEquals(rawRecords.get(5), preparedRecords.get(3));
    }

    @Test
    public void prepare_givenInvalidListOfSensorRecords_throwException() throws InvalidSensorRecordNumberException {
        ArrayList<SensorRecord> invalidRecords = new ArrayList<SensorRecord>() {{
            add(new SensorRecord("B", 100));
        }};
        assertThrows(InvalidSensorRecordNumberException.class, () -> processor.prepare(invalidRecords));
        invalidRecords.add(0, new SensorRecord("A", 100));
        invalidRecords.add(new SensorRecord("B", 100));
        assertThrows(InvalidSensorRecordNumberException.class, () -> processor.prepare(invalidRecords));
    }
}
