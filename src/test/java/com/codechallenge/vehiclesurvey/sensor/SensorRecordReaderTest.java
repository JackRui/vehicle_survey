package com.codechallenge.vehiclesurvey.sensor;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class SensorRecordReaderTest {

    @Test
    public void constructor_givenNothing_usesDefaultResource() throws FileNotFoundException {
        assertNotNull(new SensorRecordReader().getReader());
    }

    @Test
    public void constructor_givenExistingFile_usesSpecifiedFile() throws IOException {
        File temp = File.createTempFile("temp", ".txt");
        assertNotNull(new SensorRecordReader(temp.getAbsolutePath()).getReader());
        temp.deleteOnExit();
    }

    @Test
    public void constructor_givenNotExistingFile_expectException() {
        assertThrows(FileNotFoundException.class, () -> {
            new SensorRecordReader("I am an invalid file.txt");
        });
    }

    @Test
    public void readList_givenCorrectValues_readsRecords()
            throws IOException, InvalidSensorRecordFormatException, InvalidSensorRecordNumberException {

        SensorRecordReader recordReader = new SensorRecordReader();

        StringReader stringReader = new StringReader("A123123\nB321321\na123456\nb654321");
        ArrayList<SensorRecord> records = recordReader.readList(stringReader);

        assertEquals(4, records.size());
        assertEquals(Sensor.A, records.get(0).getSensor());
        assertEquals(123123, records.get(0).getMillisecond());
        assertEquals(Sensor.B, records.get(1).getSensor());
        assertEquals(321321, records.get(1).getMillisecond());
        assertEquals(Sensor.A, records.get(2).getSensor());
        assertEquals(123456, records.get(2).getMillisecond());
        assertEquals(Sensor.B, records.get(3).getSensor());
        assertEquals(654321, records.get(3).getMillisecond());
    }

    @Test
    public void readList_givenEmptyLines_readsRecordsIgnoreEmptyLine()
            throws IOException, InvalidSensorRecordFormatException, InvalidSensorRecordNumberException {
        SensorRecordReader recordReader = new SensorRecordReader();

        StringReader stringReader = new StringReader("A123123\n\n\nA321321");
        ArrayList<SensorRecord> records = recordReader.readList(stringReader);

        assertEquals(2, records.size());
        assertEquals(Sensor.A, records.get(0).getSensor());
        assertEquals(123123, records.get(0).getMillisecond());
        assertEquals(Sensor.A, records.get(1).getSensor());
        assertEquals(321321, records.get(1).getMillisecond());
    }


    @Test
    public void readList_givenIncorrectValues_throwsException()
            throws IOException, InvalidSensorRecordNumberException {
        SensorRecordReader recordReader = new SensorRecordReader();

        StringReader stringReader1 = new StringReader("C123123");
        assertThrows(InvalidSensorRecordFormatException.class, () -> recordReader.readList(stringReader1));

        StringReader stringReader2 = new StringReader("A1abcd");
        assertThrows(InvalidSensorRecordFormatException.class, () -> recordReader.readList(stringReader2));
    }

    @Test
    public void readList_givenOddValues_throwsException()
            throws IOException, InvalidSensorRecordFormatException {
        SensorRecordReader recordReader = new SensorRecordReader();
        StringReader stringReader = new StringReader("A100\nB200");
        assertThrows(InvalidSensorRecordNumberException.class, () -> recordReader.readList(stringReader));
    }
}
