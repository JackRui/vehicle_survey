package com.codechallenge.vehiclesurvey.util;

import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DistanceTest {
    @Test
    public void of_givenSpeedAndDuration_calculateSpeedMultiplyDuration() {
        assertEquals(300.0, Distance.of(Speed.ofMetersPerSecond(100), Duration.ofSeconds(3)).toMeters());
    }
    @Test
    public void of_givenZeroSpeedAndDuration_calculateSpeedMultiplyDuration() {
        assertEquals(Distance.ZERO, Distance.of(Speed.ZERO, Duration.ofSeconds(3)));
        assertEquals(Distance.ZERO, Distance.of(Speed.ofMetersPerSecond(100), Duration.ZERO));
    }

    @Test
    public void ofMeters_givenPositiveMeters_createNoneZeroDistance() {
        assertEquals(100.99, Distance.ofMeters(100.99).toMeters());
    }

    @Test
    public void ofMeters_givenZeroMeters_createZeroDistance() {
        assertEquals(Distance.ZERO, Distance.ofMeters(0));
    }


    @Test
    public void ofMeters_givenNegativeMeters_createZeroDistance() {
        assertEquals(Distance.ZERO, Distance.ofMeters(-1.00));
    }

    @Test
    public void ofKilometers_givenKilometers_setMeters() {
        assertEquals(300.0, Distance.ofKilometers(0.300).toMeters());
    }

    @Test
    public void ofMiles_givenMiles_setMeters() {
        assertEquals(1609.344, Distance.ofMiles(1).toMeters());
    }

    @Test
    public void toKilometers_givenMeters_getKilometers() {
        assertEquals(0.3, Distance.ofMeters(300).toKilometers());
    }

    @Test
    public void toMiles_givenMeters_getMiles() {
        assertEquals(1, Distance.ofMeters(1609.344).toMiles());
    }

    @Test
    public void compareTo_givenMeters_returnDiff() {
        assertEquals(-1, Distance.ofMeters(1).compareTo(Distance.ofMeters(2)));
    }

    @Test
    public void equals_givenMeters_returnEquals() {
        assertTrue(Distance.ofMeters(1).equals(Distance.ofMeters(1.0000000000000000)));
        assertTrue(Distance.ofMeters(-1).equals(Distance.ZERO));
    }
}
