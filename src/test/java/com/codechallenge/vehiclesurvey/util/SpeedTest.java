package com.codechallenge.vehiclesurvey.util;

import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SpeedTest {

    @Test
    public void of_givenDistanceAndDuration_createNoneZeroSpeed() {
        assertEquals(1.99, Speed.of(Distance.ofMeters(1.99), Duration.ofSeconds(1)).toMetersPerSecond());
    }

    @Test
    public void of_givenZeroDistanceAndDuration_createZeroSpeed() {
        assertEquals(Speed.ZERO, Speed.of(Distance.ZERO, Duration.ofSeconds(1)));
        assertEquals(Speed.ZERO, Speed.of(Distance.ofMeters(1.99), Duration.ZERO));
    }

    @Test
    public void ofMetersPerSecond_givenPositiveSpeed_setSpeed() {
        assertEquals(1.9999, Speed.ofMetersPerSecond(1.9999).toMetersPerSecond());
        assertEquals(1.9999 * 3.6, Speed.ofMetersPerSecond(1.9999).toKilometersPerHour());
    }

    @Test
    public void ofMetersPerSecond_givenZeroSpeed_setZero() {
        assertEquals(Speed.ZERO, Speed.ofMetersPerSecond(0));
    }


    @Test
    public void ofMetersPerSecond_givenNegativeSpeed_setZero() {
        assertEquals(Speed.ZERO, Speed.ofMetersPerSecond(-1000));
    }

    @Test void toKilometersPerHour_givenDistanceAndDuration_calculatesSpeed(){
        assertEquals(7.164, Speed.of(Distance.ofMeters(1.99), Duration.ofSeconds(1)).toKilometersPerHour());
    }

    @Test
    public void compareTo_givenAnotherSpeed_returnDiff() {
        assertEquals(-1, Speed.ofMetersPerSecond(1).compareTo(Speed.ofMetersPerSecond(2)));
    }

    @Test
    public void equals_givenAnotherSpeed_returnEquals() {
        assertTrue(Speed.ofMetersPerSecond(1.00000000000000001).equals(Speed.ofMetersPerSecond(1.00000000000000001)));
        assertTrue(Speed.ofMetersPerSecond(-1).equals(Speed.ZERO));
    }
}
