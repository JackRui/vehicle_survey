package com.codechallenge.vehiclesurvey.util;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.TemporalUnit;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TimeUtilTest {

    private final LocalTime startLess = LocalTime.ofNanoOfDay(TimeUnit.MILLISECONDS.toNanos(1));
    private final LocalTime startGreater =
            LocalTime.ofNanoOfDay(TimeUnit.DAYS.toNanos(1) - TimeUnit.MILLISECONDS.toNanos(1));
    private final LocalTime end = LocalTime.ofNanoOfDay(TimeUnit.HOURS.toNanos(1));

    @Test
    public void getDurationBetween_givenStarLessThanEnd_calculatesDurationFromStarToEnd() {
        // between 00:00:00.001 and 01:00:00.000
        assertEquals(
                TimeUnit.MILLISECONDS.toNanos(3599999),
                TimeUtil.getDurationBetween(startLess, end).toNanos());
    }

    @Test
    public void getDurationBetween_givenStarGreaterThanEnd_addsOneDayToEndAndCalculatesDuration() {
        // between 23:59:59.999 and 01:00:00.000
        assertEquals(
                TimeUnit.MILLISECONDS.toNanos(3600001),
                TimeUtil.getDurationBetween(startGreater, end).toNanos());
    }
}
