package com.codechallenge.vehiclesurvey.report;

import com.codechallenge.vehiclesurvey.sensor.SensorRecord;
import com.codechallenge.vehiclesurvey.vehicle.InvalidDayRecordException;
import com.codechallenge.vehiclesurvey.vehicle.InvalidSensorRecordPairException;
import com.codechallenge.vehiclesurvey.vehicle.VehicleRecord;
import com.codechallenge.vehiclesurvey.vehicle.VehicleRecordBuilder;

import java.util.ArrayList;
import java.util.Arrays;

public class ReportTestData {
    public static double getExpectedDay1ASpeed() {
        return 6.75;
    }

    public static double getExpectedDay1BSpeed() {
        return 1.8;
    }

    public static double getExpectedDay2ASpeed() {
        return 9;
    }

    public static double getExpectedDay1ADistance() {
        return 2.5;
    }

    public static ArrayList<VehicleRecord> getVehicleRecords() {
        try {
            VehicleRecord vehicleRecord1 = new VehicleRecordBuilder()
                    .frontAxleSensorRecord(new SensorRecord("A", 86390000))
                    .rearAxleSensorRecord(new SensorRecord("A", 86391000))
                    .nextVehicleSensorRecord(new SensorRecord("A", 86392000))
                    .dayOfRecord(1)
                    .build();

            VehicleRecord vehicleRecord2 = new VehicleRecordBuilder()
                    .frontAxleSensorRecord(new SensorRecord("A", 86392000))
                    .rearAxleSensorRecord(new SensorRecord("A", 86394000))
                    .nextVehicleSensorRecord(new SensorRecord("A", 3600000))
                    .dayOfRecord(1)
                    .build();

            VehicleRecord vehicleRecord3 = new VehicleRecordBuilder()
                    .frontAxleSensorRecord(new SensorRecord("B", 86390000))
                    .rearAxleSensorRecord(new SensorRecord("B", 86395000))
                    .dayOfRecord(1)
                    .build();

            VehicleRecord vehicleRecord4 = new VehicleRecordBuilder()
                    .frontAxleSensorRecord(new SensorRecord("A", 1000))
                    .rearAxleSensorRecord(new SensorRecord("A", 2000))
                    .dayOfRecord(2)
                    .build();

            return new ArrayList<>(Arrays.asList(vehicleRecord1, vehicleRecord2, vehicleRecord3, vehicleRecord4));
        } catch (InvalidSensorRecordPairException | InvalidDayRecordException e) {
            e.printStackTrace();
            return null;
        }
    }
}
