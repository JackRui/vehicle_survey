package com.codechallenge.vehiclesurvey.report;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReportIntervalTest {
    @Test
    public void getValue_givenItem_getsNanoSecondsOfInterval() {
        assertEquals(900000000000L, ReportInterval.NANOS_PER_15_MINUTE.getValue());
        assertEquals(1200000000000L, ReportInterval.NANOS_PER_20_MINUTE.getValue());
        assertEquals(1800000000000L, ReportInterval.NANOS_PER_30_MINUTE.getValue());
        assertEquals(3600000000000L, ReportInterval.NANOS_PER_60_MINUTE.getValue());
        assertEquals(43200000000000L, ReportInterval.NANOS_PER_HALF_DAY.getValue());
        assertEquals(86400000000000L, ReportInterval.NANOS_PER_DAY.getValue());
    }
}
