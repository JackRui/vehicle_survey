package com.codechallenge.vehiclesurvey.report;

import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReportProcessorTest {

    @Test
    public void generateDailyReports_givenVehicleRecordsListAndInterval_generatesReports() {
        ReportProcessor reportProcessor = new ReportProcessor();
        HashMap<Integer, ReportPerDay> reports = reportProcessor.generateReports(
                ReportTestData.getVehicleRecords(), ReportInterval.NANOS_PER_60_MINUTE);
        assertEquals(2, reports.size());
        assertEquals(3, reports.get(1).getVehicleCounts());
        assertEquals(1, reports.get(2).getVehicleCounts());
    }
}
