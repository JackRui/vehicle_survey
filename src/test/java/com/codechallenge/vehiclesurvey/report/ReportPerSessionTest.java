package com.codechallenge.vehiclesurvey.report;

import com.codechallenge.vehiclesurvey.vehicle.Direction;
import com.codechallenge.vehiclesurvey.vehicle.VehicleRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ReportPerSessionTest {

    private ArrayList<VehicleRecord> vehicleRecords;
    private ReportPerSession reportPerSession;

    @BeforeEach
    public void setUp() {
        vehicleRecords = ReportTestData.getVehicleRecords();
        assertNotNull(vehicleRecords);
        reportPerSession = new ReportPerSession(
                LocalTime.ofNanoOfDay(0), LocalTime.ofNanoOfDay(TimeUnit.HOURS.toNanos(1)));
        assertNotNull(vehicleRecords);
    }

    @Test
    public void constructor_givenStartEnd_createsObject() {
        ReportPerSession reportPerSession = new ReportPerSession(
                LocalTime.ofNanoOfDay(0), LocalTime.ofNanoOfDay(TimeUnit.HOURS.toNanos(1)));
        assertEquals(2, reportPerSession.getDirectionReports().size());
    }

    @Test
    public void addAll_givenVehicleList_addsRecordsToDirectionReportItem() {
        reportPerSession.addAll(vehicleRecords);
        assertEquals(3, reportPerSession.getDirectionReports().get(Direction.NORTH).getVehicleRecords().size());
        assertEquals(1, reportPerSession.getDirectionReports().get(Direction.SOUTH).getVehicleRecords().size());
    }

    @Test
    public void analyze_givenVehicleList_calculatesInformation() {
        reportPerSession.addAll(Arrays.asList(
                vehicleRecords.get(0), vehicleRecords.get(1), vehicleRecords.get(2)
        ));
        reportPerSession.analyze();
        assertEquals(3, reportPerSession.getVehicleCounts());
    }

    @Test
    @EnabledOnOs(OS.WINDOWS)
    public void toString_givenVehicleListOnWin_convertsToStringAfterAnalyze() {
        reportPerSession.addAll(Arrays.asList(
                vehicleRecords.get(0), vehicleRecords.get(1), vehicleRecords.get(2)
        ));
        reportPerSession.analyze();
        assertEquals("00:00-01:00        Total           3\r\n" +
                "           |       NORTH           2         6.75kph           2.50m\r\n" +
                "           |       SOUTH           1         1.80kph           0.00m", reportPerSession.toString());
    }

    @Test
    @EnabledOnOs({OS.LINUX, OS.MAC})
    public void toString_givenVehicleListNonWin_convertsToStringAfterAnalyze() {
        reportPerSession.addAll(Arrays.asList(
                vehicleRecords.get(0), vehicleRecords.get(1), vehicleRecords.get(2)
        ));
        reportPerSession.analyze();
        assertEquals("00:00-01:00        Total           3\n" +
                "           |       NORTH           2         6.75kph           2.50m\n" +
                "           |       SOUTH           1         1.80kph           0.00m", reportPerSession.toString());
    }
}
