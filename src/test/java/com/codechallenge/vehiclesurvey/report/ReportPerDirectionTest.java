package com.codechallenge.vehiclesurvey.report;

import com.codechallenge.vehiclesurvey.vehicle.Direction;
import com.codechallenge.vehiclesurvey.vehicle.VehicleRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ReportPerDirectionTest {

    private ArrayList<VehicleRecord> vehicleRecords;
    private ReportPerDirection reportPerDirection;

    @BeforeEach
    public void setUp() {
        vehicleRecords = ReportTestData.getVehicleRecords();
        reportPerDirection = new ReportPerDirection(
                LocalTime.ofNanoOfDay(0), LocalTime.ofNanoOfDay(TimeUnit.HOURS.toNanos(1)), Direction.NORTH);
        assertNotNull(vehicleRecords);
    }

    @Test
    public void constructor_givenStartEnd_createsObject() {
        ReportPerDirection reportPerDirection = new ReportPerDirection(
                LocalTime.ofNanoOfDay(0), LocalTime.ofNanoOfDay(TimeUnit.HOURS.toNanos(1)), Direction.SOUTH);
        assertEquals(LocalTime.parse("00:00:00"), reportPerDirection.getStart());
        assertEquals(LocalTime.parse("01:00:00"), reportPerDirection.getEnd());
        assertEquals(Direction.SOUTH, reportPerDirection.getDirection());
        assertEquals(0, reportPerDirection.getVehicleCounts());
        assertEquals(0, reportPerDirection.getAverageSpeedInKph());
        assertEquals(0, reportPerDirection.getAverageDistanceInMeter());
        assertEquals(0, reportPerDirection.getVehicleRecords().size());
    }

    @Test
    public void addAll_givenVehicleList_addsRecords(){
        reportPerDirection.addAll(vehicleRecords);
        assertEquals(4,reportPerDirection.getVehicleRecords().size());
    }

    @Test
    public void analyze_givenVehicleList_calculatesInformation(){
        reportPerDirection.addAll(Arrays.asList(
                vehicleRecords.get(0), vehicleRecords.get(1)
        ));
        reportPerDirection.analyze();
        assertEquals(2,reportPerDirection.getVehicleCounts());
        assertEquals(ReportTestData.getExpectedDay1ASpeed(), reportPerDirection.getAverageSpeedInKph());
        assertEquals(ReportTestData.getExpectedDay1ADistance(), reportPerDirection.getAverageDistanceInMeter());
    }

    @Test
    public void toString_givenVehicleList_convertsToStringAfterAnalyze(){
        reportPerDirection.addAll(Arrays.asList(
                vehicleRecords.get(0), vehicleRecords.get(1)
        ));
        reportPerDirection.analyze();
        assertEquals("       NORTH           2         6.75kph           2.50m", reportPerDirection.toString());
    }
}
