package com.codechallenge.vehiclesurvey.report;

import com.codechallenge.vehiclesurvey.vehicle.Direction;
import com.codechallenge.vehiclesurvey.vehicle.VehicleRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

public class ReportPerDayTest {
    private ArrayList<VehicleRecord> vehicleRecords;
    private ReportPerDay reportPerDay;

    @BeforeEach
    public void setUp() {
        vehicleRecords = ReportTestData.getVehicleRecords();
        reportPerDay = new ReportPerDay(0,ReportInterval.NANOS_PER_60_MINUTE);
        assertNotNull(vehicleRecords);
    }

    @Test
    public void constructor_givenDayAndInterval_createsObject() {
        ReportPerDay reportPerDay = new ReportPerDay(1,ReportInterval.NANOS_PER_15_MINUTE);
        assertEquals(96,reportPerDay.getReportSessions().size());

        reportPerDay = new ReportPerDay(1,ReportInterval.NANOS_PER_20_MINUTE);
        assertEquals(72,reportPerDay.getReportSessions().size());

        reportPerDay = new ReportPerDay(1,ReportInterval.NANOS_PER_30_MINUTE);
        assertEquals(48,reportPerDay.getReportSessions().size());

        reportPerDay = new ReportPerDay(1,ReportInterval.NANOS_PER_60_MINUTE);
        assertEquals(24,reportPerDay.getReportSessions().size());

        reportPerDay = new ReportPerDay(1,ReportInterval.NANOS_PER_HALF_DAY);
        assertEquals(2,reportPerDay.getReportSessions().size());

        reportPerDay = new ReportPerDay(1,ReportInterval.NANOS_PER_DAY);
        assertEquals(1,reportPerDay.getReportSessions().size());
    }

    /**
     * Test if data os correctly allocated to the sub report items
     */
    @Test
    public void addAll_givenVehicleList_addsRecordsToSubReportItems() {
        reportPerDay.addAll(vehicleRecords);
        assertEquals(1, reportPerDay.getReportSessions().get(0)
                .getDirectionReports().get(Direction.NORTH).getVehicleRecords().size());
        assertEquals(2, reportPerDay.getReportSessions().get(23)
                .getDirectionReports().get(Direction.NORTH).getVehicleRecords().size());
        assertEquals(1, reportPerDay.getReportSessions().get(23)
                .getDirectionReports().get(Direction.SOUTH).getVehicleRecords().size());
    }

    @Test
    public void analyze_givenVehicleList_calculatesTotalCountsAndPeakSessions() {
        ArrayList<VehicleRecord> testRecords = ReportTestData.getVehicleRecords();
        vehicleRecords.addAll(Arrays.asList(
                testRecords.get(3),testRecords.get(3)));
        reportPerDay.addAll(vehicleRecords);
        reportPerDay.analyze();
        HashMap<Integer, ReportPerSession> sessionHashMap = reportPerDay.getReportSessions();
        assertEquals(6, reportPerDay.getVehicleCounts());

        assertTrue(reportPerDay.getPeakSessions().contains(sessionHashMap.get(0)));
        assertTrue(reportPerDay.getPeakSessions().contains(sessionHashMap.get(23)));

        assertTrue(reportPerDay.getNorthPeakSessions().contains(sessionHashMap.get(0)));
        assertFalse(reportPerDay.getNorthPeakSessions().contains(sessionHashMap.get(23)));

        assertFalse(reportPerDay.getSouthPeakSessions().contains(sessionHashMap.get(0)));
        assertTrue(reportPerDay.getSouthPeakSessions().contains(sessionHashMap.get(23)));
    }

    @Test
    @EnabledOnOs(OS.WINDOWS)
    public void toString_givenVehicleListOnWin_convertsToStringAfterAnalyze(){
        reportPerDay.addAll(vehicleRecords);
        reportPerDay.analyze();
        String s = "* DAY   0    Total: 4\r\n" +
                "*            Peak Times: 23:00-23:59, \r\n" +
                "*            North Peaks: 23:00-23:59, \r\n" +
                "*            South Peaks: 23:00-23:59, ";

        assertEquals(s, reportPerDay.toString());
    }

    @Test
    @EnabledOnOs({OS.MAC, OS.LINUX})
    public void toString_givenVehicleListNonWIn_convertsToStringAfterAnalyze(){
        reportPerDay.addAll(vehicleRecords);
        reportPerDay.analyze();
        String s = "* DAY   0    Total: 4\n" +
                "*            Peak Times: 23:00-23:59, \n" +
                "*            North Peaks: 23:00-23:59, \n" +
                "*            South Peaks: 23:00-23:59, ";

        assertEquals(s, reportPerDay.toString());
    }
}
