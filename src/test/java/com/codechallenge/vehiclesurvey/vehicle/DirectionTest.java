package com.codechallenge.vehiclesurvey.vehicle;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DirectionTest {
    @Test
    public void valueOfDirection_givenExpectedValues_convertsToDirection() {
        assertNotNull(Direction.valueOf("NORTH"));
        assertNotNull(Direction.valueOf("SOUTH"));
    }
}
