package com.codechallenge.vehiclesurvey.vehicle;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class VehicleRecordTest {

    // for coverage purpose but meaningless
    @Test
    public void constructor_createsObject(){
        assertNotNull(new VehicleRecord());
    }
}
