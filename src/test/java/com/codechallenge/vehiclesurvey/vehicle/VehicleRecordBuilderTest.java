package com.codechallenge.vehiclesurvey.vehicle;

import com.codechallenge.vehiclesurvey.sensor.SensorRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

public class VehicleRecordBuilderTest {

    private VehicleRecordBuilder builder;

    @BeforeEach
    public void setUp() {
        builder = new VehicleRecordBuilder();
    }

    // Tests for setters are not covered

    @Test
    public void build_givenCorrectSensorRecords_createsVehicleRecord() throws InvalidSensorRecordPairException, InvalidDayRecordException {
        VehicleRecord record = builder.dayOfRecord(1)
                .frontAxleSensorRecord(new SensorRecord("A", 100))
                .rearAxleSensorRecord(new SensorRecord("A", 1100))
                .nextVehicleSensorRecord(new SensorRecord("A", 21100))
                .build();
        assertEquals(1, record.getDay());
        assertEquals(Direction.NORTH, record.getDirection());
        assertEquals(LocalTime.ofNanoOfDay(100 * 1000000), record.getTimeOfRecord());
        assertEquals(9, record.getSpeedInKph());
        assertEquals(Double.valueOf(50), record.getMetersToNextCar());
    }

    @Test
    public void build_givenSensorRecordsWithoutFollowingVehicle_createsVehicleRecord()
            throws InvalidSensorRecordPairException, InvalidDayRecordException {
        VehicleRecord record = builder.dayOfRecord(2)
                .frontAxleSensorRecord(new SensorRecord("B", 100))
                .rearAxleSensorRecord(new SensorRecord("B", 1100))
                .build();
        assertEquals(2, record.getDay());
        assertEquals(Direction.SOUTH, record.getDirection());
        assertEquals(LocalTime.ofNanoOfDay(100 * 1000000), record.getTimeOfRecord());
        assertEquals(9, record.getSpeedInKph());
        assertNull(record.getMetersToNextCar());
    }

    @Test
    public void build_givenInvalidDayRecord_throwsException() throws InvalidSensorRecordPairException {
        assertThrows(InvalidDayRecordException.class, () -> builder.build());
        assertThrows(InvalidDayRecordException.class, () -> builder.dayOfRecord(-100).build());
    }

    @Test
    public void build_givenInvalidSensorRecords_throwsException() throws InvalidDayRecordException {
        assertThrows(InvalidSensorRecordPairException.class, () ->
                builder.dayOfRecord(1).frontAxleSensorRecord(new SensorRecord("A", 100)).build());

        assertThrows(InvalidSensorRecordPairException.class, () ->
                builder.dayOfRecord(1).rearAxleSensorRecord(new SensorRecord("B", 100)).build());

        assertThrows(InvalidSensorRecordPairException.class, () ->
                builder.dayOfRecord(1)
                        .frontAxleSensorRecord(new SensorRecord("A", 100))
                        .rearAxleSensorRecord(new SensorRecord("B", 100))
                        .build());
    }

    @Test
    public void build_givenRearRecordTimeEarlierThanFront_createsVehicleRecordWithDayIncrement()
            throws InvalidSensorRecordPairException, InvalidDayRecordException {
        VehicleRecord record = builder.dayOfRecord(1)
                .frontAxleSensorRecord(
                        new SensorRecord("A",
                                (int) (TimeUnit.HOURS.toMillis(23)
                                        + TimeUnit.MINUTES.toMillis(59)
                                        + TimeUnit.SECONDS.toMillis(59))))
                .rearAxleSensorRecord(
                        new SensorRecord("A", (int) TimeUnit.SECONDS.toMillis(1)))
                .build();
        assertEquals(4.5, record.getSpeedInKph());
    }

    @Test
    public void build_givenNextVehicleRecordTimeEarlierThanRear_createsVehicleRecordWithDayIncrement()
            throws InvalidSensorRecordPairException, InvalidDayRecordException {
        VehicleRecord record = builder.dayOfRecord(1)
                .frontAxleSensorRecord(
                        new SensorRecord("A",
                                (int) (TimeUnit.HOURS.toMillis(23)
                                        + TimeUnit.MINUTES.toMillis(59)
                                        + TimeUnit.SECONDS.toMillis(58))))
                .rearAxleSensorRecord(
                        new SensorRecord("A",
                                (int) (TimeUnit.HOURS.toMillis(23)
                                        + TimeUnit.MINUTES.toMillis(59)
                                        + TimeUnit.SECONDS.toMillis(59))))
                .nextVehicleSensorRecord(
                        new SensorRecord("A", (int) TimeUnit.SECONDS.toMillis(1)))
                .build();
        assertEquals(9, record.getSpeedInKph());
        assertEquals(Double.valueOf(5), record.getMetersToNextCar());
    }
}
