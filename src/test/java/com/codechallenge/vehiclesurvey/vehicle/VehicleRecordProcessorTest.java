package com.codechallenge.vehiclesurvey.vehicle;

import com.codechallenge.vehiclesurvey.sensor.SensorRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class VehicleRecordProcessorTest {

    private ArrayList<SensorRecord> sensorRecords;
    private VehicleRecordProcessor processor;

    @BeforeEach
    public void setUp() {
        processor = new VehicleRecordProcessor();
        sensorRecords = new ArrayList<>();
        sensorRecords.addAll(Arrays.asList(
                new SensorRecord("A", 86390000), // car 1 axle 1
                new SensorRecord("A", 86390500), // car 1 axle 2
                new SensorRecord("B", 86398100), // car 2 axle 1
                new SensorRecord("A", 2000),     // car 3 axle 1
                new SensorRecord("B", 2100),
                new SensorRecord("A", 3000)));
    }

    @Test
    public void prepareBuilders_givenSensorRecords_createsBuilders() {
        ArrayList<VehicleRecordBuilder> builders = processor.prepareBuilders(sensorRecords);
        assertEquals(3, builders.size());

        assertEquals(sensorRecords.get(0), builders.get(0).getFrontAxleRecord());
        assertEquals(sensorRecords.get(1), builders.get(0).getRearAxleRecord());
        assertEquals(sensorRecords.get(3), builders.get(0).getNextVehicleRecord());

        assertEquals(sensorRecords.get(2), builders.get(1).getFrontAxleRecord());
        assertEquals(sensorRecords.get(4), builders.get(1).getRearAxleRecord());
        assertNull(builders.get(1).getNextVehicleRecord());

        assertEquals(sensorRecords.get(3), builders.get(2).getFrontAxleRecord());
        assertEquals(sensorRecords.get(5), builders.get(2).getRearAxleRecord());
        assertNull(builders.get(2).getNextVehicleRecord());
    }

    /**
     * Dependency test case here. For easy test, not creating mock data.
     * Only test day value and nulls, other record content test are covered by VehicleRecordBuilderTest
     */
    @Test
    public void buildRecords_givenValidBuilders_buildsRecords()
            throws InvalidDayRecordException, InvalidSensorRecordPairException {

        ArrayList<VehicleRecordBuilder> builders = processor.prepareBuilders(sensorRecords);
        ArrayList<VehicleRecord> records = processor.buildRecords(builders);

        assertEquals(3, records.size());

        assertEquals(1, records.get(0).getDay());
        assertNotNull(records.get(0).getMetersToNextCar());

        assertEquals(1, records.get(1).getDay());
        assertNull(records.get(1).getMetersToNextCar());

        assertEquals(2, records.get(2).getDay());
        assertNull(records.get(2).getMetersToNextCar());
    }
}
