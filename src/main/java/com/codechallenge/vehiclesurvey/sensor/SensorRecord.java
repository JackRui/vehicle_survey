package com.codechallenge.vehiclesurvey.sensor;

import java.time.LocalTime;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SensorRecord {
    private static final String SENSOR_RECORD_PATTERN = "^([ABab])([0-9]+)$";
    private static final Pattern PATTERN = Pattern.compile(SENSOR_RECORD_PATTERN);

    private Sensor sensor;
    private int millisecond;
    private LocalTime localTime;

    public SensorRecord(String hose, int millisecond) {
        this.sensor = Sensor.valueOf(hose);
        this.millisecond = millisecond;
        this.localTime = LocalTime.ofNanoOfDay(TimeUnit.MILLISECONDS.toNanos(millisecond));
    }

    public Sensor getSensor() {
        return sensor;
    }

    public int getMillisecond() {
        return millisecond;
    }

    public LocalTime getLocalTime() {
        return localTime;
    }

    /**
     * Parse string value to sensor record
     * @param recordInString a string with format ^([ABab])([0-9]+)$
     * @return A SensorRecord object
     * @throws InvalidSensorRecordFormatException if provided string does not match the pattern
     */
    public static SensorRecord parse(String recordInString) throws InvalidSensorRecordFormatException {
        Matcher m = PATTERN.matcher(recordInString);
        if (m.find()) {
            int millis;
            try {
                millis = Integer.parseInt(m.group(2));
            } catch (NumberFormatException e) {
                throw new InvalidSensorRecordFormatException(recordInString);
            }
            return new SensorRecord(m.group(1).toUpperCase(), millis);
        } else {
            throw new InvalidSensorRecordFormatException(recordInString);
        }
    }

}
