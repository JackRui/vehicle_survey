package com.codechallenge.vehiclesurvey.sensor;

import com.codechallenge.vehiclesurvey.util.TimeUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class SensorRecordProcessor {

    /**
     * Prepare a record list that can reflect the truth.
     * Remove redundant sensor A records, since sensor B has already recorded.
     * <p>
     * Find a pair of B records, match each A records between Bs with A records before the first B
     *
     * @param rawRecords Records read from a file.
     * @return A list in which sensor A only associates with Northbound and sensor B for Southbound.
     * @throws InvalidSensorRecordNumberException if there is not an A record between 2 B records.
     */
    public ArrayList<SensorRecord> prepare(ArrayList<SensorRecord> rawRecords)
            throws InvalidSensorRecordNumberException {
        ArrayList<SensorRecord> redundantRecords = new ArrayList<>();
        int lastBIndex = -1;
        int cntB = 0;
        for (int i = 0; i < rawRecords.size(); i++) {
            if (Sensor.B.equals(rawRecords.get(i).getSensor())) {
                cntB++;
                if (cntB == 1) {
                    lastBIndex = i;
                    if (lastBIndex == 0) throw new InvalidSensorRecordNumberException();
                }
                if (cntB == 2) {
                    if (i - lastBIndex <= 1) throw new InvalidSensorRecordNumberException();
                    redundantRecords.addAll(identifyRedundantAFromB(rawRecords, lastBIndex, i));
                    cntB = 0;
                }
            }
        }
        rawRecords.removeAll(redundantRecords);
        return rawRecords;
    }

    public ArrayList<SensorRecord> identifyRedundantAFromB(
            ArrayList<SensorRecord> rawRecords, int lastBIndex, int currentBIndex) throws InvalidSensorRecordNumberException {

        long intervalB = TimeUtil.getDurationBetween(
                rawRecords.get(lastBIndex).getLocalTime(),
                rawRecords.get(currentBIndex).getLocalTime())
                .toMillis();

        HashMap<Long, ArrayList<SensorRecord>> matchedRecordMap = new HashMap<>();
        Long minDiff = Long.MAX_VALUE;
        for (int i = currentBIndex - 1; i > lastBIndex; i--) {
            SensorRecord end = rawRecords.get(i); // this has to be A sensor
            for (int j = lastBIndex - 1; j >= 0; j--) {
                SensorRecord start = rawRecords.get(j);
                if (!Sensor.A.equals(start.getSensor())) {
                    break;
                }
                long intervalA = TimeUtil.getDurationBetween(start.getLocalTime(), end.getLocalTime()).toMillis();
                long diff = Math.abs(intervalA - intervalB);
                if (diff < minDiff && !matchedRecordMap.containsKey(diff)) {
                    matchedRecordMap.remove(minDiff);
                    ArrayList<SensorRecord> matchedRecords = new ArrayList<>();
                    matchedRecords.add(start);
                    matchedRecords.add(end);
                    matchedRecordMap.put(diff, matchedRecords);
                    minDiff = diff;
                } else {
                    break;
                }
            }
            minDiff = Long.MAX_VALUE;
        }
        if (matchedRecordMap.isEmpty()) {
            throw new InvalidSensorRecordNumberException("Expecting A records before B.");
        }
        return matchedRecordMap.get(Collections.min(matchedRecordMap.keySet()));
    }
}
