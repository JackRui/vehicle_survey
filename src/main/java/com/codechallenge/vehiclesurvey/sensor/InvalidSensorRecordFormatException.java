package com.codechallenge.vehiclesurvey.sensor;

public class InvalidSensorRecordFormatException extends Exception {
    public InvalidSensorRecordFormatException(String record) {
        super(String.format("Unexpected format in sensor record - %s.", record));
    }
}
