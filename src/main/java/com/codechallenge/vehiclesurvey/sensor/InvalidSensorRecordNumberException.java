package com.codechallenge.vehiclesurvey.sensor;

public class InvalidSensorRecordNumberException extends Exception {
    public InvalidSensorRecordNumberException() {
        super("The number of sensor records should be even.");
    }
    public InvalidSensorRecordNumberException(String msg) {
        super(msg);
    }
}
