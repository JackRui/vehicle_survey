package com.codechallenge.vehiclesurvey.sensor;


import java.io.*;
import java.util.ArrayList;

public class SensorRecordReader {

    private static final String DEFAULT_RES_FILE = "/sample.txt";
    private Reader reader;

    public SensorRecordReader() {
        InputStream inputStream = this.getClass().getResourceAsStream(DEFAULT_RES_FILE);
        this.reader = new InputStreamReader(inputStream);
    }

    public SensorRecordReader(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        this.reader = new FileReader(file);
    }

    public ArrayList<SensorRecord> readList(Reader reader)
            throws IOException, InvalidSensorRecordFormatException, InvalidSensorRecordNumberException {
        ArrayList<SensorRecord> records = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(reader);
        String line;
        int countB = 0;
        while ((line = bufferedReader.readLine()) != null) {
            if (!line.isEmpty()) {
                SensorRecord sensorRecord = SensorRecord.parse(line);
                records.add(sensorRecord);
                countB += Sensor.B.equals(sensorRecord.getSensor()) ? 1 : 0;
            }
        }
        if (countB % 2 != 0 || records.size() % 2 != 0) throw new InvalidSensorRecordNumberException();
        return records;
    }


    public Reader getReader() {
        return reader;
    }
}
