package com.codechallenge.vehiclesurvey;

import com.codechallenge.vehiclesurvey.report.ReportInterval;
import com.codechallenge.vehiclesurvey.report.ReportPerDay;
import com.codechallenge.vehiclesurvey.report.ReportPerSession;
import com.codechallenge.vehiclesurvey.report.ReportProcessor;
import com.codechallenge.vehiclesurvey.sensor.*;
import com.codechallenge.vehiclesurvey.vehicle.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class VehicleSurveyApp {
    public static final String USAGE = String.format(
            "Usage: %n\tAppend [pathToFile] as a parameter to specify a file to read.%n%s%n%s",
            "\tIf not specified, the app will use the embedded sample file.",
            "\tReports will be generated in the same directory.");

    public static final String OUTPUT_DIR = "reports";

    public static void main(String[] args) {
        if (args.length > 0) {
            if (Arrays.asList(args).contains("-h")) {
                System.out.println(USAGE);
            } else {
                new VehicleSurveyApp(args[0]).start();
            }
        } else {
            new VehicleSurveyApp().start();
        }
    }

    private String inputFile;
    public String getInputFile(){
        return inputFile;
    }
    public VehicleSurveyApp(String inputFile) {
        this.inputFile = inputFile;
    }

    public VehicleSurveyApp() {
        this.inputFile = "";
    }

    // can replace to logger
    public void println(String content){
        System.out.println(content);
    }

    public void start() {
        try {
            println(String.format("Start reading file %s...", inputFile));
            // read from file
            SensorRecordReader recordReader = (inputFile == null || inputFile.isEmpty())
                    ? new SensorRecordReader()
                    : new SensorRecordReader(inputFile);
            ArrayList<SensorRecord> sensorRecords = recordReader.readList(recordReader.getReader());
            println(String.format("Read %d raw records. Processing ...", sensorRecords.size()));
            // precess raw records
            SensorRecordProcessor sensorRecordProcessor = new SensorRecordProcessor();
            ArrayList<SensorRecord> processedSensorRecords = sensorRecordProcessor.prepare(sensorRecords);
            println(String.format("Obtained %d vehicle axle records. Building vehicle records ...",
                    processedSensorRecords.size()));
            // build vehicle records
            VehicleRecordProcessor vehicleRecordProcessor = new VehicleRecordProcessor();
            ArrayList<VehicleRecordBuilder> builders =
                    vehicleRecordProcessor.prepareBuilders(processedSensorRecords);
            ArrayList<VehicleRecord> vehicleRecords = vehicleRecordProcessor.buildRecords(builders);
            println(String.format("Built %d vehicle records. Generating reports ...", vehicleRecords.size()));
            // generate reports for all interval
            ReportProcessor reportProcessor = new ReportProcessor();
            createOutputDirectory(OUTPUT_DIR);
            for (ReportInterval interval : ReportInterval.values()) {
                // generate reports with specified interval
                HashMap<Integer, ReportPerDay> dailyReports = reportProcessor.generateReports(vehicleRecords, interval);

                String intervalStr = interval.name().replace("NANOS_", "");
                println(String.format("Writing reports with interval %s", intervalStr));
                String fileName = String.format("%s/Report_%s.txt", OUTPUT_DIR, intervalStr);
                // write to file
                outputReportsToFile(dailyReports, fileName);
            }
            println("Finished!");
        } catch (IOException
                | InvalidSensorRecordFormatException
                | InvalidSensorRecordNumberException
                | InvalidDayRecordException
                | InvalidSensorRecordPairException e) {
            println("An error occurred!");
            println(e.getMessage());
            //e.printStackTrace();
        }
    }

    public void createOutputDirectory(String dir){
        new File(dir).mkdirs();
    }

    public void outputReportsToFile(HashMap<Integer, ReportPerDay> reports, String fileName) throws IOException {
        File file = new File(fileName);
        if(!file.exists())file.createNewFile();

        //overwrite existing file
        FileWriter fw = new FileWriter(file, false);
        BufferedWriter writer = new BufferedWriter(fw);

        List<Integer> dayIndexes = reports.keySet().stream().sorted().collect(Collectors.toList());
        for (Integer i : dayIndexes) {
            ReportPerDay report = reports.get(i);
            writer.write(report.toString());
            writer.newLine();
            writer.write(ReportPerSession.SESSION_TABLE_HEADER);//writing a table header
            writer.newLine();
            HashMap<Integer, ReportPerSession> sessions = report.getReportSessions();
            for (int j = 0; j < sessions.size(); j++) {
                writer.write(sessions.get(j).toString());
                writer.newLine();
            }
        }
        writer.close();
    }
}
