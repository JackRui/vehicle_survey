package com.codechallenge.vehiclesurvey.vehicle;

import com.codechallenge.vehiclesurvey.sensor.Sensor;
import com.codechallenge.vehiclesurvey.sensor.SensorRecord;

import java.util.ArrayList;
import java.util.HashMap;

public class VehicleRecordProcessor {

    /**
     * Traverse the builders and set day value to each vehicle record, then built the records.
     *
     * @param builders A list of builders for vehicle records
     * @return A list of built vehicle records
     * @throws InvalidDayRecordException if any of the builder does not set day
     * @throws InvalidSensorRecordPairException if sensor are different in axle records
     */
    public ArrayList<VehicleRecord> buildRecords(ArrayList<VehicleRecordBuilder> builders)
            throws InvalidDayRecordException, InvalidSensorRecordPairException {
        ArrayList<VehicleRecord> records = new ArrayList<>();
        int day = 1;
        for (int i = 0; i < builders.size(); i++) {
            if (i > 1 && builders.get(i - 1).getFrontAxleRecord().getLocalTime()
                    .isAfter(builders.get(i).getFrontAxleRecord().getLocalTime())) {
                day++;
            }
            builders.get(i).dayOfRecord(day);
            records.add(builders.get(i)
                    .dayOfRecord(day)
                    .build());
        }
        return records;
    }

    /**
     *  Generate a list of builder based on a series of SensorRecords.
     *  The input should be pre-processed, meaning it does not contain redundant records.
     *  All A type sensor will be considered as Northbound traffic, and B type for Southbound.
     *
     * @param processedSensorRecords A list of sensor records, typically the result of SensorRecordProcessor.prepare()
     * @return A list of builders ready to create records.
     */
    public ArrayList<VehicleRecordBuilder> prepareBuilders(ArrayList<SensorRecord> processedSensorRecords) {
        ArrayList<VehicleRecordBuilder> builderList = new ArrayList<>();
        // stores counters for even/odd judgement
        HashMap<Sensor, Integer> counterMap = new HashMap<>();
        counterMap.put(Sensor.A, 0);
        counterMap.put(Sensor.B, 0);
        // the last index of builder for certain type of sensor
        HashMap<Sensor, Integer> lastIndexMap = new HashMap<>();
        lastIndexMap.put(Sensor.A, -1);
        lastIndexMap.put(Sensor.B, -1);

        // when odd, create builder for new record and update last record
        // when even, finish setting values
        for (SensorRecord currentSensorRecord : processedSensorRecords) {
            Sensor sensorKey = currentSensorRecord.getSensor();
            counterMap.put(sensorKey, counterMap.get(sensorKey) + 1);
            int counter = counterMap.get(sensorKey);
            if (counter % 2 == 1) {
                if (counter > 1) {
                    builderList.get(lastIndexMap.get(sensorKey)).nextVehicleSensorRecord(currentSensorRecord);
                }
                builderList.add(new VehicleRecordBuilder().frontAxleSensorRecord(currentSensorRecord));
                lastIndexMap.put(sensorKey, builderList.size() - 1);
            } else {
                builderList.get(lastIndexMap.get(sensorKey)).rearAxleSensorRecord(currentSensorRecord);
            }
        }
        return builderList;
    }
}
