package com.codechallenge.vehiclesurvey.vehicle;

public class InvalidSensorRecordPairException extends Exception {
    public InvalidSensorRecordPairException(){
        super("Unexpected sensor records are set.");
    }
}
