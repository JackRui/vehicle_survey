package com.codechallenge.vehiclesurvey.vehicle;

import java.time.LocalTime;

public class VehicleRecord {

    private Direction direction;
    private int day;
    // need to be converted if stored in db
    // use front axle record time
    private LocalTime timeOfRecord;
    private double speedInKph;
    private Double metersToNextCar;

    protected VehicleRecord() {
    }

    public Direction getDirection() {
        return direction;
    }

    public int getDay() {
        return day;
    }

    public LocalTime getTimeOfRecord() {
        return timeOfRecord;
    }

    public double getSpeedInKph() {
        return speedInKph;
    }

    public Double getMetersToNextCar() {
        return metersToNextCar;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setTimeOfRecord(LocalTime timeOfRecord) {
        this.timeOfRecord = timeOfRecord;
    }

    public void setSpeedInKph(double speedInKph) {
        this.speedInKph = speedInKph;
    }

    public void setMetersToNextCar(Double metersToNextCar) {
        this.metersToNextCar = metersToNextCar;
    }
}
