package com.codechallenge.vehiclesurvey.vehicle;

public enum Direction {
    NORTH,
    SOUTH
}
