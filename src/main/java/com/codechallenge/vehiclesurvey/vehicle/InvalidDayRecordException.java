package com.codechallenge.vehiclesurvey.vehicle;

public class InvalidDayRecordException extends Throwable {
    public InvalidDayRecordException(){
        super("Unexpected day records are set.");
    }
}
