package com.codechallenge.vehiclesurvey.vehicle;

import com.codechallenge.vehiclesurvey.sensor.Sensor;
import com.codechallenge.vehiclesurvey.sensor.SensorRecord;
import com.codechallenge.vehiclesurvey.util.Distance;
import com.codechallenge.vehiclesurvey.util.Speed;
import com.codechallenge.vehiclesurvey.util.TimeUtil;

import java.time.Duration;

public class VehicleRecordBuilder {

    private final static double METERS_BETWEEN_AXLES = 2.5;

    private int dayOfRecord = -1;
    private SensorRecord frontAxleRecord;
    private SensorRecord rearAxleRecord;
    private SensorRecord nextVehicleRecord;

    public VehicleRecordBuilder dayOfRecord(int day) {
        this.dayOfRecord = day;
        return this;
    }

    public VehicleRecordBuilder frontAxleSensorRecord(SensorRecord frontAxleRecord) {
        this.frontAxleRecord = frontAxleRecord;
        return this;
    }

    public VehicleRecordBuilder rearAxleSensorRecord(SensorRecord rearAxleRecord) {
        this.rearAxleRecord = rearAxleRecord;
        return this;
    }

    public VehicleRecordBuilder nextVehicleSensorRecord(SensorRecord frontAxleRecordOfNext) {
        this.nextVehicleRecord = frontAxleRecordOfNext;
        return this;
    }

    public SensorRecord getFrontAxleRecord() {
        return frontAxleRecord;
    }

    public SensorRecord getRearAxleRecord() {
        return rearAxleRecord;
    }

    public SensorRecord getNextVehicleRecord() {
        return nextVehicleRecord;
    }

    public int getDayOfRecord() {
        return dayOfRecord;
    }

    /**
     * Build the vehicle record, checking mandatory records.
     * It is not allowed if day is set less than 0 or two sensors are different.
     * It is allowed not having vehicle behind
     * This methods will calculate static information for statistical purpose.
     *
     * @return
     * @throws InvalidSensorRecordPairException
     * @throws InvalidDayRecordException
     */
    public VehicleRecord build() throws InvalidSensorRecordPairException, InvalidDayRecordException {
        if (dayOfRecord < 0) {
            throw new InvalidDayRecordException();
        }
        if (frontAxleRecord == null || rearAxleRecord == null
                || !frontAxleRecord.getSensor().equals(rearAxleRecord.getSensor())) {
            throw new InvalidSensorRecordPairException();
        }

        Duration durationBetweenAxles =
                TimeUtil.getDurationBetween(frontAxleRecord.getLocalTime(), rearAxleRecord.getLocalTime());
        Speed speed = Speed.of(Distance.ofMeters(METERS_BETWEEN_AXLES), durationBetweenAxles);

        // vehicle behind
        Double estimatedMetersToNextVehicle = null;
        if (nextVehicleRecord != null) {
            Duration durationBetweenVehicles =
                    TimeUtil.getDurationBetween(rearAxleRecord.getLocalTime(), nextVehicleRecord.getLocalTime());
            estimatedMetersToNextVehicle = Distance.of(speed, durationBetweenVehicles).toMeters();
        }

        // Build the object
        VehicleRecord record = new VehicleRecord();
        record.setDay(dayOfRecord);
        record.setDirection(Sensor.B.equals(frontAxleRecord.getSensor()) ? Direction.SOUTH : Direction.NORTH);
        record.setTimeOfRecord(frontAxleRecord.getLocalTime());
        record.setSpeedInKph(speed.toKilometersPerHour());
        record.setMetersToNextCar(estimatedMetersToNextVehicle);
        return record;
    }
}
