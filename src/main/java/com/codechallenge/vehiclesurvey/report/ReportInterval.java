package com.codechallenge.vehiclesurvey.report;


public enum ReportInterval {
    NANOS_PER_15_MINUTE(900000000000L),
    NANOS_PER_20_MINUTE(1200000000000L),
    NANOS_PER_30_MINUTE(1800000000000L),
    NANOS_PER_60_MINUTE(3600000000000L),
    NANOS_PER_HALF_DAY(43200000000000L),
    NANOS_PER_DAY(86400000000000L);

    private final long interval;

    ReportInterval(long interval) {
        this.interval = interval;
    }

    public long getValue() {
        return interval;
    }
}