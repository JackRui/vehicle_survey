package com.codechallenge.vehiclesurvey.report;

import com.codechallenge.vehiclesurvey.util.TimeUtil;
import com.codechallenge.vehiclesurvey.vehicle.Direction;
import com.codechallenge.vehiclesurvey.vehicle.VehicleRecord;

import java.time.LocalTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Report for certain time period, including different directions.
 */
public class ReportPerSession extends AbstractReportItem {

    public static final String SESSION_TABLE_HEADER=String.format("%1$11s| %2$12s%3$12s%4$16s%5$16s",
            "Session","Direction","Counts","Avg. speed", "Avg. distance");

    private HashMap<Direction, ReportPerDirection> directionReports;

    public ReportPerSession(LocalTime start, LocalTime end) {
        this.start = start;
        this.end = end;
        this.directionReports = new HashMap<>();
        this.directionReports.put(Direction.NORTH, new ReportPerDirection(start, end, Direction.NORTH));
        this.directionReports.put(Direction.SOUTH, new ReportPerDirection(start, end, Direction.SOUTH));
    }

    /**
     * Distribute vehicle records to different directions
     * @param records a collection of vehicle records
     */
    @Override
    public void addAll(Collection<VehicleRecord> records) {
        Map<Direction, List<VehicleRecord>> classifiedRecords = records.stream().collect(Collectors.groupingBy(
                VehicleRecord::getDirection,
                Collectors.toList()
        ));
        classifiedRecords.forEach((k, v) -> {
            directionReports.get(k).addAll(v);
        });
    }

    /**
     * Average speed and distance are not calculated, because they are less meaningful with mixed directions.
     * @return itself
     */
    @Override
    public ReportPerSession analyze() {
        vehicleCounts = 0;
        directionReports.forEach((k, v) -> {
            v.analyze();
            vehicleCounts += v.getVehicleCounts();
        });
        return this;
    }

    @Override
    public String toString() {
        return String.format("%1$11s %2$12s%3$12d%n%4$12s%5$s%n%6$12s%7$s",
                String.format("%s-%s",start.format(TimeUtil.SHORT_FORMATTER), end.format(TimeUtil.SHORT_FORMATTER))
                , "Total", vehicleCounts,
                "|", directionReports.get(Direction.NORTH).toString(),
                "|", directionReports.get(Direction.SOUTH).toString());
    }

    // getters
    public HashMap<Direction, ReportPerDirection> getDirectionReports() {
        return directionReports;
    }
}
