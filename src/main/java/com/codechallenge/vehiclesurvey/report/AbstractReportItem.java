package com.codechallenge.vehiclesurvey.report;

import com.codechallenge.vehiclesurvey.vehicle.VehicleRecord;

import java.time.LocalTime;
import java.util.Collection;

/**
 * Abstract report item, can be structured to be a tree-like report.
 */
public abstract class AbstractReportItem {
    protected LocalTime start;
    protected LocalTime end;
    protected int vehicleCounts;

    public abstract void addAll(Collection<VehicleRecord> records);

    public abstract AbstractReportItem analyze();

    public abstract String toString();

    public LocalTime getStart() {
        return start;
    }

    public LocalTime getEnd() {
        return end;
    }

    public int getVehicleCounts() {
        return vehicleCounts;
    }

}
