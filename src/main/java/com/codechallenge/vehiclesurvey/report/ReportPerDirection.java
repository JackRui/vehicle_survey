package com.codechallenge.vehiclesurvey.report;

import com.codechallenge.vehiclesurvey.vehicle.Direction;
import com.codechallenge.vehiclesurvey.vehicle.VehicleRecord;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Report of certain direction in certain period
 */
public class ReportPerDirection extends AbstractReportItem {

    private ArrayList<VehicleRecord> vehicleRecords;
    private Direction direction;
    private double averageSpeedInKph;
    private double averageDistanceInMeter;

    public ReportPerDirection(LocalTime start, LocalTime end, Direction direction) {
        this.start = start;
        this.end = end;
        this.direction = direction;
        this.vehicleRecords = new ArrayList<>();
    }

    @Override
    public void addAll(Collection<VehicleRecord> records) {
        vehicleRecords.addAll(records);
    }

    /**
     * Count cars,
     * Calculate the average car speed and distance on the current direction
     * @return itself
     */
    @Override
    public ReportPerDirection analyze() {
        vehicleCounts = vehicleRecords.size();

        double totalSpeed = 0;
        double totalDistance = 0;
        for (int i = 0; i < vehicleCounts; i++) {
            VehicleRecord vehicleRecord = vehicleRecords.get(i);
            totalSpeed += vehicleRecord.getSpeedInKph();
            // last car in the session does not count the distance
            if (i != vehicleCounts - 1) {
                Double meters = vehicleRecord.getMetersToNextCar();
                totalDistance += meters == null ? 0 : meters;
            }
        }
        averageSpeedInKph = vehicleCounts > 0 ? totalSpeed / vehicleCounts : 0;
        averageDistanceInMeter = vehicleCounts > 1 ? totalDistance / (vehicleCounts - 1) : 0;
        return this;
    }

    @Override
    public String toString() {
        return String.format("%1$12s%2$12d%3$13.2fkph%4$15.2fm",
                direction.name(), vehicleCounts, averageSpeedInKph, averageDistanceInMeter);
    }

    // getters
    public Direction getDirection() {
        return direction;
    }
    public double getAverageSpeedInKph() {
        return averageSpeedInKph;
    }

    public double getAverageDistanceInMeter() {
        return averageDistanceInMeter;
    }

    public ArrayList<VehicleRecord> getVehicleRecords() {
        return vehicleRecords;
    }

}
