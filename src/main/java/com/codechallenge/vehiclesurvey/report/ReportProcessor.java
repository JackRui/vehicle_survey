package com.codechallenge.vehiclesurvey.report;

import com.codechallenge.vehiclesurvey.vehicle.VehicleRecord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ReportProcessor {

    /**
     * Use input vehicle records to generate a full report with specified session interval.
     * It will not create reports for all intervals.
     * @param vehicleRecords Pre-processed vehicle records,
     *                       typically the result of VehicleRecordProcessor.buildRecords()
     * @param interval Specify the statistical time interval
     * @return A series of daily reports for input records.
     */
    public HashMap<Integer, ReportPerDay> generateReports(
            ArrayList<VehicleRecord> vehicleRecords, ReportInterval interval) {
        Map<Integer, List<VehicleRecord>> classifiedRecords = vehicleRecords.stream()
                .collect(Collectors.groupingBy(VehicleRecord::getDay, Collectors.toList()));
        HashMap<Integer, ReportPerDay> dailyReports = new HashMap<>();
        classifiedRecords.forEach((k, v) -> {
            dailyReports.put(k, new ReportPerDay(k, interval));
            dailyReports.get(k).addAll(v);
            dailyReports.get(k).analyze();
        });
        return dailyReports;
    }
}
