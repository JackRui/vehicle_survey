package com.codechallenge.vehiclesurvey.report;

import com.codechallenge.vehiclesurvey.util.TimeUtil;
import com.codechallenge.vehiclesurvey.vehicle.Direction;
import com.codechallenge.vehiclesurvey.vehicle.VehicleRecord;

import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Report for a day, with sub report items for different time period.
 */
public class ReportPerDay extends AbstractReportItem {
    private int day;
    private ReportInterval sessionInterval;
    private HashMap<Integer, ReportPerSession> reportSessions;
    private List<ReportPerSession> peakSessions;
    private List<ReportPerSession> northPeakSessions;
    private List<ReportPerSession> southPeakSessions;

    public ReportPerDay(int day, ReportInterval interval) {
        this.start = LocalTime.MIN;
        this.end = LocalTime.MAX;
        this.day = day;
        this.sessionInterval = interval;
        this.reportSessions = new HashMap<>();
        long numberOfSessions = ReportInterval.NANOS_PER_DAY.getValue() / interval.getValue();
        for (int i = 0; i < numberOfSessions; i++) {
            LocalTime sessionStart = LocalTime.ofNanoOfDay(interval.getValue() * i);
            LocalTime sessionEnd = LocalTime.ofNanoOfDay(interval.getValue() * (i + 1) - 1);
            this.reportSessions.put(i, new ReportPerSession(sessionStart, sessionEnd));
        }
        this.peakSessions = new ArrayList<>();
        this.northPeakSessions = new ArrayList<>();
        this.southPeakSessions = new ArrayList<>();
    }

    public int getSessionIndexFromTime(LocalTime localTime) {
        return (int) (localTime.toNanoOfDay() / sessionInterval.getValue());
    }

    private List<ReportPerSession> getPeakSessionsFromCollection(
            Collection<ReportPerSession> sessions, Direction direction) {
        int peakCount = sessions.stream()
                .mapToInt(r -> r.getDirectionReports().get(direction).getVehicleCounts()).max().orElse(-1);
        if (peakCount > 0) {
            return sessions.stream()
                    .filter(r -> r.getDirectionReports().get(direction).getVehicleCounts() == peakCount)
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    /**
     * Distribute vehicle records to different time sessions
     * @param records a collection of vehicle records
     */
    @Override
    public void addAll(Collection<VehicleRecord> records) {
        Map<Integer, List<VehicleRecord>> classifiedRecords = records.stream().collect(Collectors.groupingBy(
                r -> getSessionIndexFromTime(r.getTimeOfRecord()),
                Collectors.toList()));
        classifiedRecords.forEach((k, v) -> {
            reportSessions.get(k).addAll(v);
        });
    }

    /**
     * Calculate everything necessary for one day report.
     * Only after running this method, the property values could be set.
     * @return itself
     */
    @Override
    public ReportPerDay analyze() {

        vehicleCounts = 0;
        reportSessions.forEach((k, report) -> {
            report.analyze();
            vehicleCounts += report.getVehicleCounts();
        });
        Collection<ReportPerSession> sessionList = reportSessions.values();
        // peak session, less considered on performance
        int peakCount = sessionList.stream().mapToInt(ReportPerSession::getVehicleCounts).max().orElse(-1);
        if (peakCount > 0) {
            peakSessions = sessionList.stream()
                    .filter(r -> r.getVehicleCounts() == peakCount)
                    .collect(Collectors.toList());
        }
        northPeakSessions = getPeakSessionsFromCollection(sessionList, Direction.NORTH);
        southPeakSessions = getPeakSessionsFromCollection(sessionList, Direction.SOUTH);

        // counts
        vehicleCounts = sessionList.stream().mapToInt(ReportPerSession::getVehicleCounts).sum();
        return this;
    }

    @Override
    public String toString() {
        return String.format("* DAY %1$3d    Total: %2$d%n*            Peak Times: %3$s%n*%4$23s: %5$s%n*%6$23s: %7$s",
                day, vehicleCounts,
                getStartEndTimeString(peakSessions),
                "North Peaks", getStartEndTimeString(northPeakSessions),
                "South Peaks", getStartEndTimeString(southPeakSessions));
    }

    private String getStartEndTimeString(List<ReportPerSession>sessions) {
        StringBuilder sBuilder = new StringBuilder();
        for (int i = 0; i < sessions.size(); i++) {
            if (i == 3) { // more than 3 peak times
                sBuilder.append(" ...");
                break;
            }
            ReportPerSession session = sessions.get(i);
            sBuilder.append(String.format("%s-%s, ",
                    session.getStart().format(TimeUtil.SHORT_FORMATTER),
                    session.getEnd().format(TimeUtil.SHORT_FORMATTER)));
        }
        return sBuilder.toString();
    }

    // getters
    public int getDay() {
        return day;
    }

    public ReportInterval getSessionInterval() {
        return sessionInterval;
    }

    public HashMap<Integer, ReportPerSession> getReportSessions() {
        return reportSessions;
    }

    public List<ReportPerSession> getPeakSessions() {
        return peakSessions;
    }

    public List<ReportPerSession> getNorthPeakSessions() {
        return northPeakSessions;
    }

    public List<ReportPerSession> getSouthPeakSessions() {
        return southPeakSessions;
    }
}
