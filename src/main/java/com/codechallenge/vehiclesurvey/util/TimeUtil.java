package com.codechallenge.vehiclesurvey.util;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

public class TimeUtil {
    public static final DateTimeFormatter SHORT_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");
    /**
     * Calculate the duration from start time to end time.
     * If end is earlier than start, end time is considered to be in the next day.
     *
     * @param start LocalTime of start time point
     * @param end   LocalTime of end time point
     * @return The duration between start time and end time
     */
    public static Duration getDurationBetween(LocalTime start, LocalTime end) {
        if (end.isBefore(start)) {
            return Duration.between(start, LocalTime.MAX)
                    .plus(Duration.between(LocalTime.MIN, end))
                    .plusNanos(1); // add one missing nano
        } else {
            return Duration.between(start, end);
        }
    }
}
