package com.codechallenge.vehiclesurvey.util;
import java.time.Duration;


public class Distance implements Comparable<Distance> {

    public static Distance ZERO = new Distance(0);
    public static double METERS_PER_MILE = 1609.344;
    public static double METERS_PER_KILO = 1000;

    private static Distance create(double meters) {
        if (meters <= 0) {
            return ZERO;
        }
        return new Distance(meters);
    }

    public static Distance of(Speed speed, Duration duration) {
        return create(speed.toMetersPerSecond() * (duration.toNanos() / Speed.NANOS_PER_SECOND));
    }

    /**
     * Convert a positive double value of meters to a distance
     *
     * @param meters
     * @return
     */
    public static Distance ofMeters(double meters) {
        return create(meters);
    }

    /**
     * Convert a positive double value of kilometers to a distance
     *
     * @param kilometers
     * @return
     */
    public static Distance ofKilometers(double kilometers) {
        return create(kilometers * METERS_PER_KILO);
    }

    /**
     * Convert a positive double value of miles to a distance
     *
     * @param miles
     * @return
     */
    public static Distance ofMiles(double miles) {
        return create(miles * METERS_PER_MILE);
    }

    private final double meters;

    private Distance(double meters) {
        this.meters = meters;
    }

    public double toMeters() {
        return meters;
    }

    public double toKilometers() {
        return meters / METERS_PER_KILO;
    }

    public double toMiles() {
        return meters / METERS_PER_MILE;
    }

    @Override
    public int compareTo(Distance otherDistance) {
        return Double.compare(meters, otherDistance.meters);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof Distance) {
            Distance other = (Distance) o;
            return this.meters == other.meters;
        }
        return false;
    }
}
