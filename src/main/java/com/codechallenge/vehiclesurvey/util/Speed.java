package com.codechallenge.vehiclesurvey.util;

import java.time.Duration;
import java.util.concurrent.TimeUnit;


public class Speed implements Comparable<Speed> {
    public static Speed ZERO = new Speed(0);
    public static int SECONDS_PER_HOUR = 3600;
    public static final long NANOS_PER_SECOND = TimeUnit.SECONDS.toNanos(1);

    private static Speed create(double distanceInMeters, double timeInSeconds) {
        if (distanceInMeters <= 0 || timeInSeconds <=0) {
            return ZERO;
        }
        return new Speed(distanceInMeters / timeInSeconds);
    }

    public static Speed of(Distance distance, Duration duration) {
        double seconds = ((double) duration.toNanos()) / NANOS_PER_SECOND;
        return create(distance.toMeters(), seconds);
    }

    public static Speed ofMetersPerSecond(double metersPerSecond) {
        return create(metersPerSecond, 1);
    }

    private final double metersPerSecond;

    private Speed(double metersPerSecond) {
        this.metersPerSecond = metersPerSecond;
    }

    public double toMetersPerSecond() {
        return metersPerSecond;
    }

    public double toKilometersPerHour() {
        return metersPerSecond * SECONDS_PER_HOUR / Distance.METERS_PER_KILO;
    }

    @Override
    public int compareTo(Speed o) {
        return Double.compare(metersPerSecond, o.metersPerSecond);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof Speed) {
            Speed other = (Speed) o;
            return this.metersPerSecond == other.metersPerSecond;
        }
        return false;
    }
}
